
function dbConnection(section="client")
    conf = ConfParse(joinpath(homedir(),".dbconf"))
    parse_conf!(conf)
    user     = retrieve(conf, section, "user")
    password = retrieve(conf, section, "passwd");
    host     = retrieve(conf, section, "host")
    port     = haskey(conf, section, "port") ? retrieve(conf, section, "port", Int) : 3306
    DBInterface.connect(MySQL.Connection, host, user, password; port=port, reconnect=true)
end

