using StatsBase, Distributions

ϕ(x) = filter(isfinite, skipmissing(x))

abslog(x) = sign(x)*log(abs(x))
std(x) = std(x, mean(x))
std(x,μ) = √sum((x.-μ).^2)
nanstd(x) = (z -> std(z, mean(z)))(ϕ(x))

stderr(x) = std(x)/√length(x)
nanstderr(x) = stderr(ϕ(x))
stdci(x) = mean(x) .+ [-1,1].*stderr(x)
nanstdci(x) = stdci(ϕ(x))
nanmean(x) = mean(ϕ(x))

bootci(x,F;α=0.05, boots=1000) = begin
    out = map(z->F(rand(x,length(x))), 1:boots)
    quantile(out, [α/2, 1-α/2])

end

binoci(x, α) = begin
    n = length(x)
    B = Binomial(n, sum(x)/n)
    map(z -> invlogcdf(B, log(z)), [α/2, 1-α/2])./n
end
binoci(x) = binoci(x, 0.05)
nanbinoci(x) = binoci(ϕ(x))
nanbinoci(x, α) = binoci(ϕ(x), α)

nanzscore(x) = begin
    out = copy(x)
    good = isfinite.(x) .& .!ismissing.(x)
    xg = x[good]
    out[good] = (xg .- mean(xg)) ./ std(xg)
    out
end

binnedbino(x,y, bins, μ, Ε) = begin
    h = fit(Histogram, x, bins)
    #@show h
    ox = (bins[1:end-1] + bins[2:end])/2
    xmap = StatsBase.binindex.(Ref(h), x)
    #@show xmap
    oy = [sum(z.==xmap) > 0 ? μ(y[z.==xmap]) : NaN for z in 1:length(ox)]
    oe = [sum(z.==xmap) > 0 ? Ε(y[z.==xmap]) : [NaN, NaN] for z in 1:length(ox)]
    # This returns a long list of 2-tuples, but we want a 2-tuple of vectors
    (ox, oy, 	(oy .- (x->x[1]).(oe), (x->x[2]).(oe) .- oy))
end

"""
`binned(x,y,bins, μ, ϵ)`

Takes vectors `x,y` of equal length than bins `x` according to `bins`. Apply `µ,E` (often mean) to `y` grouped by bins of `x`.

`µ` is any function that takes a vector and return a number (usually `mean`)
`E` is any function that takes a vector and returns a 2-element iterable, which represent the lower and upper CI for that bin.

Returns a 3-tuple with `(bin_c,  µ_y, E_y)` such that you can plot the results with `plot(bin_c, µ_y, yerr=E_y)`
"""
binned(x,y, bins, μ, ϵ) = begin
    @assert length(x) == length(y)
    h = fit(Histogram, x, bins)
    ox = (bins[1:end-1] + bins[2:end])/2
    xmap = StatsBase.binindex.(Ref(h), x)
    oy = [sum(z.==xmap) > 0 ? μ(y[z.==xmap]) : NaN for z in 1:length(ox)]
    oe = [sum(z.==xmap) > 0 ? ϵ(y[z.==xmap]) : [NaN, NaN] for z in 1:length(ox)]
    # This returns a long list of 2-tuples, but we want a 2-tuple of vectors
    (ox, oy, 	(oy .- (x->x[1]).(oe), (x->x[2]).(oe) .- oy))
end

guess_bins(v ; lim = 10) = begin
    u = unique(v)
    if length(u) < lim
        su = sort(u)
        return vcat([su[1]*0.8, (su[2:end] .+ su[1:end-1]) ./ 2, su[end]*1.2]...)
    else
        return quantile(v, range(0,1,length=lim))
    end
end

binned(x,y, bins) = begin
    if (eltype(y) == Bool) || all(in.(y, Ref([0,1])))
        binnedbino(x,y,bins, nanmean, nanbinoci)
    else
        binned(x,y,bins, nanmean, x->(nanstderr(x),nanstderr(x)))
    end
end




lrt(m1, m2) = begin
    if dof(m1) > dof(m2)
        redM = m2; M = m1;
    elseif dof(m1) < dof(m2)
        redM = m1; M = m2;
    else
        error("Same dof. These are not nested models")
    end
    λ = -2(loglikelihood(redM)- loglikelihood(M))
    Χ=Distributions.Chisq(dof(M) - dof(redM));
    d = [Symbol(f)=>f(x) for x in [M, redM], f in [loglikelihood, aic, bic, dof]]
    1 - cdf(Χ, λ)
    df = DataFrame(map(Dict, eachrow(d)))
    df.p = [1 - cdf(Χ, λ), NaN]
    df
end





"""
`bin2d(x,y,z, bins, μ)`

Takes vectors `x,y,z` of equal length, then bins `x` and `y` according to `bins`. 

Apply `µ,E` (often mean, stderr) to `z` grouped by bins of `x,y`.

`µ` is any function that takes a vector and return a number (usually `mean`)
`E` is any function that takes a vector and returns a 2-element iterable, which represent the lower and upper CI for that bin.

Returns a 4-tuple with `(bin_i,  µ_y, E_y, n, bx, by)` such that you can plot the results with 
`heatmap(bx,by, µ_y)` 
"""
bin2d(x::AbstractVector{<:Real},y::AbstractVector{<:Real},z::AbstractVector{<:Real},bine::AbstractVector,μ::Function=nanmean, E::Function=nanstderr) =   bin2d(x,y,z,bine,bine,μ,E)

function bin2d(x::AbstractVector{<:Real},y::AbstractVector{<:Real},z::AbstractVector{<:Real},binx::AbstractVector,biny::AbstractVector,μ::Function=nanmean, E::Function=nanstderr)
	@assert length(x) == length(y) == length(z)
    h = fit(Histogram, (x, y),(binx,biny))
	bins = StatsBase.binindex.(Ref(h), zip(x, y))
	bin_i = [(i,j) for i in 1:length(binx)-1, j in 1:length(biny)-1][:]
    binxc = (binx[1:end-1] .+ binx[2:end])./2
    binyc = (biny[1:end-1] .+ biny[2:end])./2
	o = zeros(length(bin_i))
    n = similar(o)
    e = zeros(length(bin_i),2)
	for (i,bin) ∈ enumerate(bin_i)
		o[i] = μ(z[bins .== Ref(bin)])
        e[i,:] .= E(z[bins .== Ref(bin)])
        n[i] = sum(bins .== Ref(bin)) 
	end
	bin_i, o, e, n, binxc[first.(bin_i)], binyc[last.(bin_i)]
end